package com.example.a20211123andresramosnycschools

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recyclerView: RecyclerView = findViewById(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(baseContext)
        val schoolListAdapter = SchoolListAdapter()
        recyclerView.adapter = schoolListAdapter
        val schoolViewModel = SchoolViewModel()
        schoolViewModel.getSchoolList()
        schoolViewModel.schools.observe(this) {
            it?.let {
                schoolListAdapter.setSchoolList(it)
            }
        }
        schoolViewModel.schoolSATandDetails.observe(this) {
            it?.let {
                val sat = it.first
                val details = it.second
                val intent = Intent(this, DetailActivity::class.java)
                intent.putExtra("SCHOOL_NAME", details.get("school_name")?.asString)
                intent.putExtra("DESCRIPTION", details.get("overview_paragraph")?.asString)
                intent.putExtra("PHONE_NUMBER", details.get("phone_number")?.asString)
                intent.putExtra("EMAIL", details.get("school_email")?.asString)
                intent.putExtra("WEBSITE", details.get("website")?.asString)

                if (sat.size() > 0) {
                    val mathAvgScore = sat.get(0).asJsonObject.get("sat_math_avg_score").asString
                    val writingAvgScore = sat.get(0).asJsonObject.get("sat_writing_avg_score").asString
                    val readingAvgSCore = sat.get(0).asJsonObject.get("sat_critical_reading_avg_score").asString
                    intent.putExtra("MATH_SCORE", mathAvgScore)
                    intent.putExtra("READING_SCORE", writingAvgScore)
                    intent.putExtra("WRITING_SCORE", readingAvgSCore)
                }

                startActivity(intent)
            }
        }
        schoolListAdapter.onClickListener.observe(this) {
            it?.let {
                schoolViewModel.getSchoolDetails(it)
            }
        }

    }
}