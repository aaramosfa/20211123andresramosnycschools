package com.example.a20211123andresramosnycschools

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SchoolViewModel : ViewModel() {

    val schools = MutableLiveData<List<JsonObject>>()
    val schoolSATandDetails = MutableLiveData<Pair<JsonArray, JsonObject>>()

    fun getSchoolList() {
        DataSource().getSchoolList().enqueue(object : Callback<List<JsonObject>> {
            override fun onFailure(call: Call<List<JsonObject>>, t: Throwable) {
                Log.e("tag","failure: " + t)
            }

            override fun onResponse(call: Call<List<JsonObject>>, response: Response<List<JsonObject>>) {
                if (response.isSuccessful && response.body() != null) {
                    schools.postValue(response.body())
                }
            }
        })
    }

    fun getSchoolDetails(details: JsonObject) {
        DataSource().getSchoolDetails(details.get("dbn").asString).enqueue(object : Callback<JsonArray> {
            override fun onFailure(call: Call<JsonArray>, t: Throwable) {
                Log.e("tag","failure: " + t)
            }

            override fun onResponse(call: Call<JsonArray>, response: Response<JsonArray>) {
                if (response.isSuccessful && response.body() != null) {
                    if (response.body() != null) {
                        schoolSATandDetails.postValue(Pair(response.body()!!, details))
                    }
                }
            }
        })
    }
}