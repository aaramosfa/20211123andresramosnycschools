package com.example.a20211123andresramosnycschools

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject

class SchoolListAdapter : RecyclerView.Adapter<SchoolListAdapter.ViewHolder>() {

    private var schoolList: List<JsonObject> = listOf()
    val onClickListener = MutableLiveData<JsonObject>()


    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val schoolName: TextView = view.findViewById(R.id.school_name)
        val container: ConstraintLayout = view.findViewById(R.id.container)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.school_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return schoolList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            holder.container.setBackgroundColor( if (position % 2 == 0) holder.container.context.getColor(R.color.gray) else holder.container.context.getColor(R.color.white))
        }
        holder.schoolName.text = schoolList.get(position).get("school_name").asString
        holder.container.setOnClickListener {
            onClickListener.postValue(schoolList.get(position))
        }
    }


    fun setSchoolList(list: List<JsonObject>) {
        schoolList = list
        notifyDataSetChanged()
    }

}
