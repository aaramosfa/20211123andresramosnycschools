package com.example.a20211123andresramosnycschools

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ApiInterface {

    @GET("resource/s3k6-pzi2.json")
    fun readJson(): Call<List<JsonObject>>

    @GET("resource/f9bf-2cp4.json")
    fun getSchoolDetails(@Query("dbn") dbn: String): Call<JsonArray>

}
