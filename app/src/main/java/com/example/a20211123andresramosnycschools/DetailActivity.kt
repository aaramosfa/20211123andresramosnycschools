package com.example.a20211123andresramosnycschools

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class DetailActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.selected_school_item)
        findViewById<TextView>(R.id.detail_school_name).text = intent.getStringExtra("SCHOOL_NAME")
        findViewById<TextView>(R.id.description_text).text = intent.getStringExtra("DESCRIPTION")
        findViewById<TextView>(R.id.phone_number).text = intent.getStringExtra("PHONE_NUMBER")
        findViewById<TextView>(R.id.email_text).text = intent.getStringExtra("EMAIL")
        findViewById<TextView>(R.id.website).text = intent.getStringExtra("WEBSITE")

        findViewById<TextView>(R.id.math_score).text = intent.getStringExtra("MATH_SCORE")
        findViewById<TextView>(R.id.reading_score).text = intent.getStringExtra("READING_SCORE")
        findViewById<TextView>(R.id.writing_score).text = intent.getStringExtra("WRITING_SCORE")

    }

}